package com.wangdeyang.icf;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class Q2 extends q {
    private RadioButton chkIos, chkIos2, chkIos3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q2);
        curr = qList.indexOf("Q2");
        addListenerOnChkIos();
    }

    private void configureRNextButton(){
        Button nextButton=findViewById(R.id.next_q2);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qList.remove("Q2");
                if (qList.size() == 0) {
                    curr = qList.indexOf("Q2");
                }

                if (curr != -1 && curr < qList.size()-1) {
                    // still in the loop
                    String temp = qList.get(curr);

                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);


                        startActivity(new Intent(Q2.this, cls));
                    } catch (ClassNotFoundException e) {

                        e.printStackTrace();
                    }
                }
                else if (curr != -1 && curr >= qList.size()-1) {
                    // when curr is at the end of the list and still get wrong questions
                    String str1 = "";
                    for (int i = 0; i < qList.size(); i++) {
                        str1 += qList.get(i);
                    }
                    Toast t1 = Toast.makeText(getApplicationContext(), "You are still getting these questions wrong: "+str1, Toast.LENGTH_LONG);
                    t1.setGravity(Gravity.CENTER, 0, 0);
                    t1.show();

                    String temp = qList.get(qList.size()-1);
                    // go back to the end of the list
                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);
                        startActivity(new Intent(Q2.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    // at the end of the module
                    startActivity(new Intent(Q2.this, Summary.class));
                }

            }
        });
    }
    private void configureWNextButton(){
        Button nextButton=findViewById(R.id.next_q2);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cList[1]++;

                if (curr != -1 && curr != qList.size()-1) {
                    // still in the loop
                    String temp = qList.get(curr+1);

                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);


                        startActivity(new Intent(Q2.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else if (curr != -1 && curr == qList.size()-1) {
                    // when curr is at the end of the list and still get wrong questions
                    String str1 = "";
                    for (int i = 0; i < qList.size(); i++) {
                        str1 += qList.get(i);
                    }
                    Toast t1 = Toast.makeText(getApplicationContext(), "You are still getting these questions wrong: "+str1, Toast.LENGTH_LONG);
                    t1.setGravity(Gravity.CENTER, 0, 0);
                    t1.show();

                    String temp = qList.get(0);
                    // go back to the end of the list
                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);
                        startActivity(new Intent(Q2.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public void addListenerOnChkIos() {

        chkIos = (RadioButton) findViewById(R.id.radioButton4);
        chkIos2 = (RadioButton) findViewById(R.id.radioButton5);
        chkIos3 = (RadioButton) findViewById(R.id.radioButton3);


        chkIos.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (((RadioButton) v).isChecked()) {

                    configureWNextButton();

                }

            }
        });

        chkIos2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (((RadioButton) v).isChecked()) {

                    configureRNextButton();

                }

            }
        });

        chkIos3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (((RadioButton) v).isChecked()) {

                    configureWNextButton();

                }

            }
        });
    }
}
