package com.wangdeyang.icf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;
import android.widget.TextView;

public class Q9 extends q {
    private RadioButton chkIos, chkIos2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q9);
        addListenerOnChkIos();
        curr = qList.indexOf("Q9");
    }
    private void configureRSubmitButton(){
        Button nextButton=findViewById(R.id.next_q9);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qList.remove("Q9");
                if (qList.size() == 0) {
                    curr = qList.indexOf("Q9");
                }


                if (curr != -1 && curr < qList.size()-1) {
                    // still in the loop
                    String temp = qList.get(curr);

                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);
                        startActivity(new Intent(Q9.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else if (curr != -1 && curr >= qList.size()-1) {
                    // when curr is at the end of the list and still get wrong questions
                    String str1 = "";
                    for (int i = 0; i < qList.size(); i++) {
                        str1 += qList.get(i);
                    }
                    Toast t1 = Toast.makeText(getApplicationContext(), "You are still getting these questions wrong: "+str1, Toast.LENGTH_LONG);
                    t1.setGravity(Gravity.CENTER, 0, 0);
                    t1.show();

                    String temp = qList.get(qList.size()-1);
                    // go back to the head of the list and
                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);
                        startActivity(new Intent(Q9.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else {

                    // at the end of the module
                    startActivity(new Intent(Q9.this, Summary.class));
                }
            }
        });
    }
    private void configureWSubmitButton(){
        Button nextButton=findViewById(R.id.next_q9);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cList[8]++;


                if (curr != -1 && curr < qList.size()-1) {
                    // still in the loop
                    String temp = qList.get(curr+1);

                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);
                        startActivity(new Intent(Q9.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else if (curr != -1 && curr == qList.size()-1) {
                    // when curr is at the end of the list and still get wrong questions
                    String str1 = "";
                    for (int i = 0; i < qList.size(); i++) {
                        str1 += qList.get(i);
                    }
                    Toast t1 = Toast.makeText(getApplicationContext(), "You are still getting these questions wrong: "+str1, Toast.LENGTH_LONG);
                    t1.setGravity(Gravity.CENTER, 0, 0);
                    t1.show();

                    String temp = qList.get(0);

                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);
                        startActivity(new Intent(Q9.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public void addListenerOnChkIos() {

        chkIos = (RadioButton) findViewById(R.id.radioButton21);
        chkIos2 = (RadioButton) findViewById(R.id.radioButton22);

        chkIos.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((RadioButton) v).isChecked()) {

                    configureRSubmitButton();

                }

            }
        });

        chkIos2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((RadioButton) v).isChecked()) {

                    configureWSubmitButton();

                }

            }
        });

    }
}
