package com.wangdeyang.icf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class Q7 extends q {
    private RadioButton chkIos, chkIos2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q7);
        addListenerOnChkIos();
        curr = qList.indexOf("Q7");
    }

    private void configureRNextButton(){
        Button nextButton=findViewById(R.id.next_q7);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qList.remove("Q7");
                if (qList.size() == 0) {
                    curr = qList.indexOf("Q7");
                }


                if (curr != -1 && curr < qList.size()-1) {
                    // still in the loop
                    String temp = qList.get(curr);

                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);
                        startActivity(new Intent(Q7.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else if (curr != -1 && curr >= qList.size()-1) {
                    // when curr is at the end of the list and still get wrong questions
                    String str1 = "";
                    for (int i = 0; i < qList.size(); i++) {
                        str1 += qList.get(i);
                    }
                    Toast t1 = Toast.makeText(getApplicationContext(), "You are still getting these questions wrong: "+str1, Toast.LENGTH_LONG);
                    t1.setGravity(Gravity.CENTER, 0, 0);
                    t1.show();

                    String temp = qList.get(qList.size()-1);

                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);
                        startActivity(new Intent(Q7.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    // at the end of the module
                    startActivity(new Intent(Q7.this, Summary.class));
                }

            }
        });
    }
    private void configureWNextButton(){
        Button nextButton=findViewById(R.id.next_q7);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cList[6]++;


                if (curr != -1 && curr < qList.size()-1) {
                    // still in the loop
                    String temp = qList.get(curr+1);

                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);
                        startActivity(new Intent(Q7.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else if (curr != -1 && curr == qList.size()-1) {
                    // when curr is at the end of the list and still get wrong questions
                    String str1 = "";
                    for (int i = 0; i < qList.size(); i++) {
                        str1 += qList.get(i);
                    }
                    Toast t1 = Toast.makeText(getApplicationContext(), "You are still getting these questions wrong: "+str1, Toast.LENGTH_LONG);
                    t1.setGravity(Gravity.CENTER, 0, 0);
                    t1.show();

                    String temp = qList.get(0);

                    try {
                        Class cls = Class.forName("com.wangdeyang.icf." + temp);
                        startActivity(new Intent(Q7.this, cls));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public void addListenerOnChkIos() {

        chkIos = (RadioButton) findViewById(R.id.radioButton17);
        chkIos2 = (RadioButton) findViewById(R.id.radioButton18);

        chkIos.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((RadioButton) v).isChecked()) {

                    configureWNextButton();

                }

            }
        });

        chkIos2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((RadioButton) v).isChecked()) {

                    configureRNextButton();

                }

            }
        });

    }
}
