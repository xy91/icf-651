package com.wangdeyang.icf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.util.Log;


public class LogInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        configureLogInButton();
    }



    private void configureLogInButton() {
        Button logInButton = findViewById(R.id.logIn);
        EditText idText = findViewById(R.id.patientID);
        EditText nameText = findViewById(R.id.patientName);

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Update DB
                loginPatient(v);
                // Go to next page
                startActivity(new Intent(LogInActivity.this, MainActivity.class));
            }
        });
    }

    // Login patient with his/her id and name
    public void loginPatient(View view) {
        // Get attribute
        EditText idText = findViewById(R.id.patientID);
        EditText nameText = findViewById(R.id.patientName);
        int id = Integer.parseInt(idText.getText().toString());
        String name = nameText.getText().toString();

        // Check input
        Log.d("Login", "Patient with id-" + id + " and name-" + name + " are logged in.");



        // Connect to DB
        MyDBHandler dbHandler = new MyDBHandler(this, null, null, 1);

        // Add patient to DB
        Patient patient = new Patient(id, name);
        dbHandler.addHandler(patient);

        // Check updated DB
        Log.d("login", dbHandler.loadHandler());
    }
}
